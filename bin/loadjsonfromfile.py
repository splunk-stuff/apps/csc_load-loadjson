#!/usr/bin/env python
# coding=utf-8

import os, sys
import time
import json

sys.path.insert(0, os.path.join(os.path.dirname(__file__), "..", "lib"))
from splunklib.searchcommands import dispatch, GeneratingCommand, Configuration, Option, validators

def load_json_from_file(file_name):
  with open(file_name, 'r') as f:
    loaded_json = json.load(f)
    return loaded_json

@Configuration()
class LoadJSON(GeneratingCommand):

    """ Generates records drawn from a json file.

    ##Syntax

    .. code-block::
        loadjsonfromfile file_name=<file_name>

    ##Description

    The :code:`loadjsonfromfile` command loads :code:`json` records from :code:`file_name` located in /opt/splunk/etc/apps/csc_generating_app_loadjson/default/data/json/.

    ##Example

    .. code-block::
        | loadjsonfromfile file_name="security_domains.json"

    This example generates records from the supplied file.

    """

    file_name = Option(
        doc='''**Syntax:** **file_name=***<path>*
        **Description:** The name of JSON file located in /opt/splunk/etc/apps/csc-load_json_from_file/default/data/json/ from which the records will be loaded''',
        name='file_name', require=True)

    def generate(self):
        safe_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "default", "data", "json")
        file_path = os.path.realpath(os.path.join(safe_dir, self.file_name))
        if os.path.commonprefix([file_path, safe_dir]) != safe_dir:
            raise Exception("File needs to be located within \"{}\"".format(safe_dir))
        else:
            records = load_json_from_file(file_path)
            for record in records:
                record.update({'_time': time.time()})
                yield record

dispatch(LoadJSON, sys.argv, sys.stdin, sys.stdout, __name__)
