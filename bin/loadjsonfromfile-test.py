
import json
import os
import time

def load_json_from_file(file_path):
    with open(file_path, 'r') as f:
      loaded_json = json.load(f)
      return loaded_json

def generate(file_name):
    safe_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "default", "data", "json")
    file_path = os.path.realpath(os.path.join(safe_dir, file_name))

    if os.path.commonprefix([file_path, safe_dir]) != safe_dir:
        print("File needs to be located within {}".format(safe_dir))
    else:
        records = load_json_from_file(file_path)
        for record in records:
            record.update({'_time': time.time()})
            print(record)

generate('security_domains.json')
