# Custom Search Command - Load JSON From File

Generating command that pulls events from a json file.

## Usage

- Copy splunklib to lib folder if not installed globally [splunk-sdk-python](https://github.com/splunk/splunk-sdk-python)
- Install the app
- Run `| loadjson file_name=<file_name>` to load json from file

## Example
- File located at `./default/data/json/security_domains.json`
![Screenshot](docs/images/json.png)

- Use the spl `| loadjson file_name="security_domains.json"` to load the file.
![Screenshot](docs/images/spl.png)
